#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_H

#include "bmpshechka.h"
#include <stdlib.h>

struct image rotate(struct image const* image);

#endif

