#ifndef ASSIGNMENT_IMAGE_ROTATION_BMPSHECHKA_H
#define ASSIGNMENT_IMAGE_ROTATION_BMPSHECHKA_H

#include "image.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define DEFAULT_biSize 40
#define DEFAULT_byPlanes 1
#define DEFAULT_PixelCounts 24
#define DEFAULT_Type 0x4D42

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE ,
    READ_INVALID_BITS ,
    READ_INVALID_HEADER,
    READ_INVALID_SIZE,
    DESTROYED_IMAGE
};

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

char const* get_print_read(enum read_status st);

char const* get_print_write(enum write_status st);

enum read_status from_bmp( FILE* in, struct image* img );

enum write_status to_bmp( FILE* out, struct image const* img );

#endif
