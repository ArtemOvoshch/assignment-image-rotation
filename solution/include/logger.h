#ifndef ASSIGNMENT_IMAGE_ROTATION_LOGGER_H
#define ASSIGNMENT_IMAGE_ROTATION_LOGGER_H

#include "bmpshechka.h"
#include "file_user.h"
#include <stdio.h>
#include <stdlib.h>

struct first_log{
    struct log* last_log;
    struct log* next;
};

struct log{
    char* result_of_command;
    struct log* next;
};

struct first_log* create_logger(void);

void write_log(struct first_log* first_log, char const* message);

void print_log_journal(struct first_log* start);

void free_journal(struct first_log* first_log);
#endif //ASSIGNMENT_IMAGE_ROTATION_LOGGER_H
