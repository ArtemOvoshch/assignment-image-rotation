#ifndef ASSIGNMENT_IMAGE_ROTATION_UTILS_H
#define ASSIGNMENT_IMAGE_ROTATION_UTILS_H

#include "file_user.h"
#include "logger.h"
#include "rotate.h"
#include <stdio.h>

enum resulted{
    OK,
    FAILED
};

enum resulted read_and_save(FILE** file, const char* path, struct image* img, struct first_log* first_log);

enum resulted write_and_save(FILE** file, const char* path, struct  image const* img, struct first_log* first_log);

#endif
