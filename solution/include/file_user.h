#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_USER_H
#define ASSIGNMENT_IMAGE_ROTATION_FILE_USER_H

#define FOREACH_MODE(MODE) \
        MODE(ab)  \
        MODE(wb)   \
        MODE(rb)  \

#define GENERATE_ENUM(ENUM) ENUM,

#include <stdio.h>

enum mode_enum{
    FOREACH_MODE(GENERATE_ENUM)
};

enum open_status{
    OPEN_FAILED,
    OPEN_SUCCESS
};

enum close_status{
    CLOSE_FAILED,
    CLOSE_SUCCESS
};

char const* get_print_open(enum open_status opened);

char const* get_print_close(enum close_status closed);

enum open_status open_file(FILE** file, const char* path, enum mode_enum mode);

enum close_status close_file(FILE** file);

#endif
