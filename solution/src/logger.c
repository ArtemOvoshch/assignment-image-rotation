#include "../include/logger.h"


//please do not blame me for this ****..... Onesided list that keeps messages.

struct first_log* create_logger(void){
    struct first_log* first_log = malloc(sizeof(struct first_log));
    first_log->next = NULL;
    first_log->last_log = NULL;
    return first_log;
}

static const char* const error_message = "Currupted log";

static struct log create_log(char const* message){
    struct log log = {0};

    if(!message){
        log.result_of_command = (char*)error_message;
    }
    else {
        log.result_of_command = (char *) message;
    }

    return log;
}

void write_log(struct first_log* first_log, char const* message){
    struct log* new_log = malloc(sizeof(struct log));
    *new_log = create_log(message);

    if((first_log)&&(first_log->last_log)&&(new_log)){
        first_log->last_log->next = new_log;
        first_log->last_log = new_log;
    }
    else if((first_log)&&(!first_log->next)&&(new_log)){
        first_log->next = new_log;
        first_log->last_log = new_log;
    }
    else{
        free(new_log);
    }
}

void print_log_journal(struct first_log* start){
    struct log* current_log = start->next;

    while(current_log){
        fprintf(stderr, "%s", current_log->result_of_command);
        current_log = current_log->next;
    }
}

void free_journal(struct first_log* first_log){

    if(!first_log->next){
        free(first_log);
    }
    else {
        struct log *current = first_log->next;
        struct log *next = NULL;

        while (current->next) {
            next = current->next;
            free(current);
            current = next;
        }

        free(first_log->last_log);
        free(first_log);
    }
}
