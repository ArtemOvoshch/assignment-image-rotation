#define GENERATE_STRING(STRING) #STRING,

#include "../include/file_user.h"


static const char* const mode_string[] = {
    FOREACH_MODE(GENERATE_STRING)
};

static const char* const open_result[]={
        [OPEN_FAILED] = "Was not able to open file\n",
        [OPEN_SUCCESS] = "Open success\n",
};

static const char* const close_result[]={
        [CLOSE_FAILED] = "Was not able to close file\n",
        [CLOSE_SUCCESS] = "Close success\n",
};

//print close_file result
char const* get_print_open(enum open_status opened){
    return (char const*)open_result[opened];
}

//print close_file result
char const* get_print_close(enum close_status closed){
    return (char const*)close_result[closed];
}

//opens file with given path
enum open_status open_file(FILE** file, const char* path, enum mode_enum mode){
    *file = fopen(path, mode_string[mode]);
    return (!*file) ? OPEN_FAILED : OPEN_SUCCESS;
}

//closes file with given path
enum close_status close_file(FILE** file){
    return (fclose(*file)) ? CLOSE_FAILED : CLOSE_SUCCESS;
}

