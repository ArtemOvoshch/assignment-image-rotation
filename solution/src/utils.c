#include "../include/utils.h"

// try to open and read bmp. created function just for comfort so you should not write this pattern every time you read bmp
enum resulted read_and_save(FILE** file, const char* path, struct image* img, struct first_log* first_log){
    const enum open_status open = open_file(file, path, rb);
    write_log(first_log, get_print_open(open));

    if(open!=OPEN_SUCCESS) return FAILED;

    const enum read_status read = from_bmp(*file, img);
    write_log(first_log, get_print_read(read));

    if(read!=READ_OK){
        close_file(file);
        return FAILED;
    }

    const enum close_status close = close_file(file);
    write_log(first_log, get_print_close(close));

    if(close!=CLOSE_SUCCESS) return FAILED;

    return OK;
}

//open and write file
enum resulted write_and_save(FILE** file, const char* path, struct  image const* img, struct first_log* first_log){
    const enum open_status open = open_file(file, path, wb);
    write_log(first_log, get_print_open(open));

    if(open!=OPEN_SUCCESS) return FAILED;

    const enum write_status write = to_bmp(*file, img);
    write_log(first_log, get_print_write(write));

    if(write!=WRITE_OK){
        close_file(file);
        return FAILED;
    }

    const enum close_status close = close_file(file);
    write_log(first_log, get_print_close(close));

    if(close!=CLOSE_SUCCESS) return FAILED;

    return OK;
}

