#include "../include/rotate.h"

static uint64_t count_pixel_location(struct image const* image, uint64_t i, uint64_t j){
    return image->height*i+(image->height-j-1);
}

struct pixel get_pixel(struct image const* image, uint64_t i, uint64_t j){
    return image->data[i+j*image->width];
}

static void set_pixel(struct image* rotated, struct pixel pixel, uint64_t position){
    rotated->data[position] = pixel;
}

//rotates image
struct image rotate(struct image const* image){
    struct image rotated = create_image(image->width, image->height);
    for(uint64_t i = 0; i < image->width; i++){
        for(uint64_t j = 0; j < image->height; j++) {
            set_pixel(&rotated, get_pixel(image, i, j), count_pixel_location(image, i, j));
        }
    }
    return rotated;
}
