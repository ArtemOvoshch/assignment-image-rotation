#include "utils.h"
#include "logger.h"
#include <stdio.h>

int main( int argc, char** argv ) {
    if(argc!=3){
        return 2;
    }

    struct first_log* logging_start = create_logger();

    const char* path_input = argv[1];
    const char* path_output = argv[2];

    struct image old_image = {0};

    FILE* file_input = NULL;
    FILE* file_output = NULL;

    //read bmp_file
    enum resulted readed = read_and_save(&file_input, path_input, &old_image, logging_start);

    if(readed!=OK) {
        print_log_journal(logging_start);
    }
    else{
        struct image new_image = rotate(&old_image);

        //write rotated bmp_image
        enum resulted written = write_and_save(&file_output, path_output, (struct image const *) &new_image,
                                               logging_start);

        if (written != OK) print_log_journal(logging_start);

        free(new_image.data);
    }

    free(old_image.data);
    //Of course it should be written in file, but i do not think it will create big problems(it will, as always)
    free_journal(logging_start);

    return 0;
}
