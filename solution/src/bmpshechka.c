#include "../include/bmpshechka.h"


struct __attribute__((packed)) bmp_header{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static const char* const read_result[] = {
        [READ_OK] = "Read success\n",
        [READ_INVALID_SIGNATURE] = "Invalid signature\n", //not type
        [READ_INVALID_BITS] = "Invalid bits\n",  //size of pixels
        [READ_INVALID_HEADER] = "Invalid header\n",
        [READ_INVALID_SIZE] = "Invalid size\n",
        [DESTROYED_IMAGE] = "Read error\n"//size of file
};

static const char* const write_result[]={
        [WRITE_OK] = "Write success\n",
        [WRITE_ERROR] = "Writing error\n"
};

static uint8_t get_padding(struct image const* image){
    return (image->width) % 4 ;
}
//check if bmp_header is valid
static enum read_status check_header(struct bmp_header const* bmp){
    if(bmp->bfileSize!=bmp->bOffBits+bmp->biSizeImage){
        return READ_INVALID_SIZE;
    }
    if(bmp->bfType!=0x4D42){
        return READ_INVALID_SIGNATURE;
    }
    if(bmp->biBitCount!=24){
        return READ_INVALID_BITS;
    }
    if(bmp->biSize!=40){
        return READ_INVALID_HEADER;
    }
    return READ_OK;
}

//print errors and statuses
char const* get_print_read(enum read_status st){
    return (char const*)read_result[st];
}

char const* get_print_write(enum write_status st){
    return (char const*)write_result[st];
}

//creates bmp_header
static struct bmp_header create_bmp(struct image const* image){
    struct bmp_header new = (struct bmp_header) {0};
    new.bfType = DEFAULT_Type;
    new.biSize = DEFAULT_biSize;
    new.biBitCount = DEFAULT_PixelCounts;
    new.biHeight = image->height;
    new.biWidth = image->width;
    new.biSizeImage = (new.biWidth*sizeof(struct pixel)+(get_padding(image)))*new.biHeight;
    new.biPlanes = DEFAULT_byPlanes;
    new.bOffBits = sizeof(struct bmp_header);
    new.bfileSize = new.bOffBits + new.biSizeImage;
    return new;
}

//read bmp image from given file
enum read_status from_bmp(FILE* in, struct image* img){
    struct bmp_header bmp = {0};

    if(!fread(&bmp, sizeof(struct bmp_header), 1, in)) return DESTROYED_IMAGE;

    const enum read_status read_resulted = check_header(&bmp);

    if (read_resulted == READ_OK) {

        struct image copy_image = create_image(bmp.biHeight, bmp.biWidth);

        if(!copy_image.data){
            return DESTROYED_IMAGE;
        }

        const uint8_t padding = get_padding(&copy_image);

        for (size_t i = 0; i < copy_image.height; i++) {
            size_t res = fread(&(copy_image.data[i * (copy_image.width)]), sizeof(struct pixel), copy_image.width, in);
            int f_res = fseek(in, padding, SEEK_CUR);

            if ((res == 0)||(f_res!=0)) {
                free(copy_image.data);
                return DESTROYED_IMAGE;
            }

        }
        //Understood that doing malloc twice just to free one without using is not the greatest solution I can make
        *img = copy_image;

        return READ_OK;
    }


    return read_resulted;
}

//write bmp image to given file
enum write_status to_bmp( FILE* out, struct image const* img ){
    const struct bmp_header prepared = create_bmp(img);

    if(!fwrite(&prepared, sizeof(struct bmp_header),1,out)) return WRITE_ERROR;

    const uint8_t padding = get_padding(img);
    const uint8_t trash[3] = {0,0,0};

    for(size_t i = 0; i < (img)->height; i++){
        size_t result = fwrite(&((img)->data[i*(img)->width]), sizeof(struct pixel), (img)->width, out );

        if (result!=(img)->width){
            return WRITE_ERROR;
        }

        if(!fwrite(&trash, sizeof(uint8_t), padding, out)) return WRITE_ERROR;
    }

    return WRITE_OK;
}
