#include "../include/image.h"

//I decided to create this because there was hard to understand logic in file structures.h without bmp_header in it (since it was removed to bmpshechka.c


//counts size if image
uint32_t count_size(uint32_t height, uint32_t width){
    return height * width * sizeof(struct pixel);
}

//creates new image
struct image create_image(uint32_t height, uint32_t width){
    struct image image_new = {.height = height, .width = width};
    image_new.data = malloc(count_size(height,width));
    return image_new;
}

